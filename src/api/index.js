import axios from 'axios'
import moment from 'moment'
export function getRepos(page){
    let d = moment().subtract(30, 'days').format('YYYY-MM-DD');
    return axios.get('https://api.github.com/search/repositories?q=created:>'+d+'&sort=stars&order=desc&page='+page);
}