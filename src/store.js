import Vue from 'vue'
import Vuex from 'vuex'
import {getRepos} from "@/api"

Vue.use(Vuex)

const PROMISE_DELAY = 1000

export default new Vuex.Store({
  state: {
    repositories: [],
    page: 1
  },
  getters: {
    repositories: state => state.repositories,
    hasRepositories: state => state.repositories.length > 0,
    currentPage: state => state.page
  },
  mutations: {
    UPDATE_REPOSITORIES(state, payload){
      state.repositories = payload;
    },
    UPDATE_PAGE(state, page){
      state.page = page;
    }
  },
  actions: {
    fetchRepositories({state,commit}){
      return new Promise((resolve, reject) => {
        state.repositories = [];
        getRepos(state.page)
        .then((res) => {
          setTimeout(() => {
            commit('UPDATE_REPOSITORIES', res.data.items)
            resolve()
          }, PROMISE_DELAY)
        })
      })
      
    },
    updatePage({commit}, page){
      commit('UPDATE_PAGE', page);
    }
  }
})
